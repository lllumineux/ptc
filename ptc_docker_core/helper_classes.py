import inspect
from dataclasses import dataclass
import enum
from typing import List


class Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance


class ClassesByNamesRegistry:
    _subclasses_dict = {}

    def __init_subclass__(cls, **kwargs):
        if not inspect.isabstract(cls):
            ClassesByNamesRegistry._subclasses_dict[cls.__name__] = cls

    @classmethod
    def get_subclass_by_name(cls, name: str):
        return cls._subclasses_dict[name]


class TaskStatus(enum.Enum):
    NOT_SUBMITTED = "not_submitted"
    SKIPPED = "skipped"
    PASSED = "passed"
    FAILED = "failed"


class Effects:
    HEADER = "\033[95m"
    SKIPPED = "\033[93m"
    PASSED = "\033[92m"
    FAILED = "\033[91m"
    TRANSLUCENT = "\033[90m"
    RESET = "\033[0m"


@dataclass
class TaskExecutionInfo:
    name: str
    status: TaskStatus
    description: str
    logs: str


@dataclass
class State:
    name: str
    is_passed: bool
    tasks: List[TaskExecutionInfo]
