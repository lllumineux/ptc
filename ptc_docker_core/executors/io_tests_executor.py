import os
import subprocess
from subprocess import PIPE
from typing import Optional, List, Tuple, Iterator

from ptc_docker_core.constants import IO_TESTS_TIME_OUT, TEST_NAME_LEN, SUBMISSION_DIRECTORY_PATH, IS_DIAGNOSTIC, FAKE_ENV
from ptc_docker_core.executors.abstract_executor import AbstractExecutor
from ptc_docker_core.loggers.abstract_logger import AbstractLogger
from ptc_docker_core.helper_classes import TaskStatus, State, TaskExecutionInfo
from ptc_docker_core.utils import strip_every_line


class IOTestsExecutor(AbstractExecutor):
    """
    This class is used to run input/output tests
    """
    def __init__(
            self,
            state_name: str,
            run_cmd: str,
            file_extension: str,
            test_data_directory_path: str,
            loggers: Optional[List[AbstractLogger]] = None
    ):
        super().__init__(state_name, loggers)
        self.run_cmd = run_cmd
        self.file_extension = file_extension
        self.test_data_directory_path = test_data_directory_path

    def run_specific_test(
            self,
            test_name: str,
            task_path: str,
            in_file_path: str,
            out_file_path: str
    ) -> Tuple[bool, str, str]:
        """
        This method runs test for specific test case

        :param test_name: name of current test (e.g. 0001)
        :param task_path: path to executed file
        :param in_file_path: path to .in file (with STDIN text)
        :param out_file_path: path to .out file (with expected STDOUT text)

        :return: a tuple about specific test execution (True (passed) or False (failed), failure description, logs).
                 If a task runs successfully, it returns empty failure description and empty logs.
        """
        with open(in_file_path, "r", encoding="utf-8") as in_file:
            in_file_text = in_file.read()

        with open(out_file_path, "r", encoding="utf-8") as out_file:
            out_file_text = strip_every_line(out_file.read())

        log_temp = f"INPUT:\n{in_file_text}\nEXPECTED:\n{out_file_text}\n"

        try:
            process = subprocess.run(
                f"cat {in_file_path} | {self.run_cmd} {task_path}",
                shell=True,
                stdout=PIPE,
                stderr=PIPE,
                timeout=IO_TESTS_TIME_OUT,
                env={**os.environ, **FAKE_ENV}
            )

            if process.returncode != 0:
                return (
                    False,
                    f"on test {test_name}: error during execution",
                    f"{log_temp}ACTUAL:\n{process.stderr.decode('utf-8')}\n"
                )

            stdout_text = strip_every_line(process.stdout.decode("utf-8"))
            if stdout_text != out_file_text:
                return False, f"on test {test_name}: output mismatch", f"{log_temp}ACTUAL:\n{stdout_text}\n"

        except subprocess.TimeoutExpired:
            return False, f"on test {test_name}: timeout", log_temp

        return True, "", ""

    def run_tests(self, task_path: str, task_test_data_directory_path: str) -> Iterator[Tuple[bool, str, str]]:
        """
        This method runs all existing test for given task

        :param task_path: path to task
        :param task_test_data_directory_path: path to directory with tests

        :return: iterator instance that yields specific task execution info until iterating over all test cases
        """
        test_index = 0

        while True:
            test_index += 1
            test_name = f"{test_index}".zfill(TEST_NAME_LEN)

            in_file_path = os.path.join(task_test_data_directory_path, f"{test_name}.in")
            out_file_path = os.path.join(task_test_data_directory_path, f"{test_name}.out")

            if not (os.path.isfile(in_file_path) and os.path.isfile(out_file_path)):
                break

            yield self.run_specific_test(test_name, task_path, in_file_path, out_file_path)

    def get_task_info(self, task_name: str) -> Tuple[TaskStatus, str, str, bool]:
        """
        This method gets information about given task execution

        :param task_name: name of task
        :return: a tuple that contains task execution info:
            TaskStatus instance,
            failure description,
            logs,
            execution status: True (if status is skipped or passed) or False (if status is not_submitted or failed))
        """
        task_path = os.path.join(SUBMISSION_DIRECTORY_PATH, f"{task_name}{self.file_extension}")
        if not os.path.isfile(task_path):
            return TaskStatus.NOT_SUBMITTED, "", "", False

        task_test_data_directory_path = os.path.join(self.test_data_directory_path, task_name, "")
        if not os.path.isdir(task_test_data_directory_path):
            return TaskStatus.SKIPPED, "", "", True

        for is_passed, desc, logs in self.run_tests(task_path, task_test_data_directory_path):
            if not is_passed:
                return TaskStatus.FAILED, desc, logs, False

        return TaskStatus.PASSED, "", "", True

    def _run(self, task_names: List[str]) -> State:
        """
        This method runs IO test for every task and returns all execution info in State instance

        :param task_names: list of tasks names
        :return State: a dataсlass instance which contains all information about some testing pipeline stage
        """
        tasks_execution_info = []
        are_all_passed = True

        for task_name in task_names:
            status, description, logs, is_passed = self.get_task_info(task_name)
            are_all_passed &= is_passed
            tasks_execution_info.append(
                TaskExecutionInfo(task_name, status, description, logs if IS_DIAGNOSTIC else "")
            )

        return State(self.state_name, are_all_passed, tasks_execution_info)
