import json
import os

import pkg_resources
# No inspection is added because these imports load all classes to ClassesByNamesRegistry
# noinspection PyUnresolvedReferences
import ptc_docker_core.loggers
# noinspection PyUnresolvedReferences
import ptc_docker_core.executors

from ptc_docker_core.constants import SUBMISSION_CONFIG_FILE_PATH
from ptc_docker_core.utils import load_pipeline_config, process_pipeline_cfg_item


def main():
    pipeline_config = load_pipeline_config(
        os.environ.get("PIPELINE_CONFIG_FILE_CONTENT")
    )

    for entry_point in pkg_resources.iter_entry_points('ptc_docker'):
        entry_point.load()

    loggers_lst = process_pipeline_cfg_item(pipeline_config, "loggers")
    executors_lst = process_pipeline_cfg_item(pipeline_config, "executors", loggers=loggers_lst)

    with open(SUBMISSION_CONFIG_FILE_PATH) as config_file:
        config = json.load(config_file)

    states = []
    for executor in executors_lst:
        states.append(executor.execute(config["task_names"]))
    for logger in loggers_lst:
        logger.apply()
    exit(not all(state.is_passed for state in states))


if __name__ == "__main__":
    main()
