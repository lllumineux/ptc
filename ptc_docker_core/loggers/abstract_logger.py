import abc

from ptc_docker_core.helper_classes import Singleton, State, ClassesByNamesRegistry


class AbstractLogger(abc.ABC, Singleton, ClassesByNamesRegistry):
    """
    This class provides interface and basic functionality for all loggers
    """

    def __init__(self):
        self.states = []

    def add_state(self, state: State) -> None:
        """
        This method adds given State object to self.state

        :param state: a dataсlass instance which contains all information about some testing pipeline stage
        """
        self.states.append(state)

    @abc.abstractmethod
    def apply(self) -> None:
        """
        Abstract method. Implementations apply logging to all states that are stored in `states` instance attribute
        """
        pass
