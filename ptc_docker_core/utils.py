import collections
import os
from typing import List, Dict, Callable, Tuple, Set

import yaml
from jinja2 import Environment, BaseLoader

from ptc_docker_core.helper_classes import Effects, State, TaskStatus, ClassesByNamesRegistry


def wrap_str_with_effect(in_str: str, effect: str) -> str:
    """
    This function adds given effect to a string by surrounding it with
    special character of effect at the beginning and with special reset effect character at the end.
    In the most cases just changes the color of the text in the terminal.

    :param in_str: string to which the effect will be applied
    :param effect: special character with effect
    :return: string with effect
    """
    return f"{effect}{in_str}{Effects.RESET}" if in_str else ""


def apply_header_effect(in_str: str) -> str:
    """
    This function applies header effect to the given string (text will be purple in terminal)

    :param in_str: string to which header effect will be applied
    :return: string with header effect
    """
    return wrap_str_with_effect(in_str, Effects.HEADER)


def apply_skipped_effect(in_str: str) -> str:
    """
    This function applies skipped effect to the given string (text will be yellow in terminal)

    :param in_str: string to which skipped effect will be applied
    :return: string with skipped effect
    """
    return wrap_str_with_effect(in_str, Effects.SKIPPED)


def apply_passed_effect(in_str: str) -> str:
    """
    This function applies passed effect to the given string (text will be green in terminal)

    :param in_str: string to which passed effect will be applied
    :return: string with passed effect
    """
    return wrap_str_with_effect(in_str, Effects.PASSED)


def apply_failed_effect(in_str: str) -> str:
    """
    This function applies failed effect to the given string (text will be red in terminal)

    :param in_str: string to which failed effect will be applied
    :return: string with failed effect
    """
    return wrap_str_with_effect(in_str, Effects.FAILED)


def apply_translucent_effect(in_str: str) -> str:
    """
    This function applies translucent effect to the given string (text will be dark grey in terminal)

    :param in_str: string to which translucent effect will be applied
    :return: string with translucent effect
    """
    return wrap_str_with_effect(in_str, Effects.TRANSLUCENT)


def get_statuses_for_task_names(states: List[State]) -> Dict[str, List[TaskStatus]]:
    """
    This function creates dict that maps task names and their statuses by list of states

    :param states: list of states to work with
    :return: a dictionary that maps task names and their statuses
    """
    statuses_for_task_names = collections.defaultdict(list)

    for state in states:
        for task in state.tasks:
            statuses_for_task_names[task.name].append(task.status)

    return dict(statuses_for_task_names)


def get_task_names_by_status(
        statuses_for_task_names: Dict[str, List[TaskStatus]],
        check_func: Callable[[List[TaskStatus]], bool]
) -> Set[str]:
    """
    This function filters task names by their statuses using providing function

    :param statuses_for_task_names: a dictionary that maps task names and their statuses
    :param check_func: a function that filters task names by their statuses

    :return task_names: set of filtered task names
    """
    task_names = set()

    for task_name in statuses_for_task_names:
        if check_func(statuses_for_task_names[task_name]):
            task_names.add(task_name)

    return task_names


def get_task_names_with_effects(task_names: Set[str], effect_to_apply: Callable[[str], str]) -> str:
    """
    This function applies given effect for every string in a provided set.
    If an empty list provided it returns `None` with translucent effect.

    :param task_names: set of task names to which to apply provided effect
    :param effect_to_apply: a function that applies effect to provided string

    :return: concatenated task names with applied effect
    """
    if len(task_names) != 0:
        return ", ".join(effect_to_apply(task_name) for task_name in task_names)
    return apply_translucent_effect("None")


def get_grouped_task_names(states: List[State]) -> Tuple[Set[str], Set[str], Set[str]]:
    """
    This function groups task names by statuses (accepted, rejected, not submitted)

    :param states: list of State objects
    :return: tuple of task names that are grouped by their statuses
    """
    statuses_for_task_names = get_statuses_for_task_names(states)

    accepted_task_names = get_task_names_by_status(
        statuses_for_task_names,
        lambda statuses: all(map(lambda status: status in (TaskStatus.PASSED, TaskStatus.SKIPPED), statuses))
    )
    rejected_task_names = get_task_names_by_status(
        statuses_for_task_names,
        lambda statuses: any(map(lambda status: status == TaskStatus.FAILED, statuses))
    )
    not_submitted_task_names = get_task_names_by_status(
        statuses_for_task_names,
        lambda statuses: any(map(lambda status: status == TaskStatus.NOT_SUBMITTED, statuses))
    )

    return accepted_task_names, rejected_task_names, not_submitted_task_names


def apply_effect_by_status(in_str: str, status: TaskStatus) -> str:
    """
    This function applies effect to provided text depending on task status

    :param in_str: string to which translucent effect will be applied
    :param status: status of task
    :return: text with applied effect
    """
    if status == TaskStatus.SKIPPED:
        return apply_skipped_effect(in_str)
    elif status == TaskStatus.PASSED:
        return apply_passed_effect(in_str)
    elif status == TaskStatus.FAILED:
        return apply_failed_effect(in_str)
    return in_str


def strip_every_line(in_str: str) -> str:
    """
    This function strips every line of provided string

    :param in_str: string to strip
    :return: stripped string
    """
    return "\n".join([line.strip() for line in in_str.strip().splitlines()])


def load_pipeline_config(pipeline_template_content: str) -> Dict[str, List[Dict[str, Dict[str, str]]]]:
    """
    This function loads pipeline template from specified name and renders it providing `submission_name`

    :param pipeline_template_content: name of configuration file
    :return: rendered and loaded to python dict yaml configuration
    """
    if pipeline_template_content is None:
        raise ValueError('PIPELINE_CONFIG_FILE_CONTENT env variable can not be None')
    template = Environment(loader=BaseLoader()).from_string(pipeline_template_content)
    pipeline_file_text = template.render(
        submission_name=os.environ.get("SUBMISSION_NAME")
    )
    return yaml.load(pipeline_file_text, Loader=yaml.FullLoader)


def process_pipeline_cfg_item(pipeline_config: Dict[str, List[Dict[str, Dict[str, str]]]], key: str, *args,
                              **kwargs) -> list:
    """
    This function generates instances of configuration items and passes included params as keyword arguments.
    Also supports adding additional positional and keyword arguments.
    For now it's used for generating executors and loggers instances by dictionary from yaml-configs.

    :param pipeline_config: config dictionary with following structure:
        Name of configuration pipeline step : list of instances that need to be generated
        Each item of this list contains name of a class and dictionary with keyword params for constructor of this class
    :param key: name of configuration pipeline step
    :param args: additional positional args that need to be added to every generated instance
    :param kwargs: additional keyword args that need to be added to every generated instance

    :return: list of generated instances
    """
    result = []
    for item in pipeline_config[key]:
        class_name, cfg_dict = next(iter((item.items())))
        result.append(ClassesByNamesRegistry.get_subclass_by_name(class_name)(*args, **{**cfg_dict, **kwargs}))
    return result
