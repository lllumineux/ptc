FROM python:3.8-alpine

WORKDIR /usr/src

RUN apk --no-cache --update add build-base
COPY ./requirements.txt .
RUN pip install -r requirements.txt
COPY . .
RUN pip install . && rm -r /usr/src

ENTRYPOINT ["ptc-run"]
